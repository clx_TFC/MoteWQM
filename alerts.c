#include "alerts.h"


static bool __low_wl_alert_active  = false;
static bool __xlow_wl_alert_active = false;



void alerts_activate (int alert_id) {
    switch (alert_id) {
    case LOW_WL_ALERT_ID:
        __low_wl_alert_active = true;
        break;
    case XLOW_WL_ALERT_ID:
        __xlow_wl_alert_active = true;
        break;
    }
}


void alerts_deactivate (int alert_id) {
    switch (alert_id) {
    case LOW_WL_ALERT_ID:
        __low_wl_alert_active = false;
        break;
    case XLOW_WL_ALERT_ID:
        __xlow_wl_alert_active = false;
        break;
    }
}


bool alert_is_active (int alert_id) {
    switch (alert_id) {
    case LOW_WL_ALERT_ID:
        return __low_wl_alert_active;
    case XLOW_WL_ALERT_ID:
        return __xlow_wl_alert_active;
    default:
        return false;
    }
}
