#include "protocol.h"


const int HEADER_FIELDS_COUNT = 4;
const int HEADER_LENGTH = 6;
const int HEADER_FIELDS[] = {
    2, // mote id
    1, // packet number
    1, // packet type
    2, // packet length
};

const int ACK_FIELDS_COUNT = 0;
const int ACK_FIELDS[] = {};

const int NACK_FIELDS_COUNT = 0;
const int NACK_FIELDS[] = {};

const int ERROR_FIELDS_COUNT = 2;
const int ERROR_FIELDS[] = {
    5, // date and time of the error
    1, // error code
};

const int INIT_FIELDS_COUNT = 1;
const int INIT_FIELDS[] = {
    4, // ip
};

const int STATUS_REPORT_FIELDS_COUNT = 6;
const int STATUS_REPORT_FIELDS[] = {
    5, // date and time of the start of the report period
    5, // date and time of the end of the report period
    2, // number of transferred packets
    2, // number of lost packets
    4, // number of transferred bytes
    4, // number of lost bytes
};

const int MEASUREMENTS_FIELDS_COUNT = 2;
const int MEASUREMENTS_FIELDS[] = {
    1, // flags
    5, // date and time of the measurements
    // plus measurements (variable sizes)
};

const int ACTION_RESULT_FIELDS_COUNT = 4;
const int ACTION_RESULT_FIELDS[] = {
    5, // date and time of the action execution
    1, // actuator interface number
    4, // action number
    1, // result code
};

const int CONF_CHANGE_FIELDS_COUNT = 1;
const int CONF_CHANGE_FIELDS[] = {
    4, // new ip
};

const int ALERT_FIELDS_COUNT = 2;
const int ALERT_FIELDS[] = {
    5, // date and time of the alert
    4, // alert identifier
    // plus measurements (variable sizes)
};

const int TRIGGER_FIELDS_COUNT = 5;
const int TRIGGER_FIELDS[] = {
    5, // date and time of the trigger occurrence
    1, // interface of the actuator called
    4, // number of the action executed
    1, // action result
    4, // identifier of the trigger
    // plus measurements (variable sizes)
};

const int CONF_SET_FIELDS_COUNT = 4;
const int CONF_SET_FIELDS[] = {
    4, // measurement period
    4, // send data period
    4, // report period
    1, // encrypt packets?
};

const int ACTION_INSTR_FIELDS_COUNT = 2;
const int ACTION_INSTR_FIELDS[] = {
    1, // actuator interface
    4, // action to perform
};

const int REQUEST_MEASUREMENTS_FIELDS_COUNT = 0;
const int REQUEST_MEASUREMENTS_FIELDS[] = {};

const int REQUEST_STATUS_REPORT_FIELDS_COUNT = 0;
const int REQUEST_STATUS_REPORT_FIELDS[] = {};

const int ALERT_SET_FIELDS_COUNT = 1;
const int ALERT_SET_FIELDS[] = {
    4, // alert identififer
};

const int ALERT_REMOVE_FIELDS_COUNT = 1;
const int ALERT_REMOVE_FIELDS[] = {
    4, // alert identififer
};

const int TRIGGER_SET_FIELDS_COUNT = 1;
const int TRIGGER_SET_FIELDS[] = {
    4, // trigger identififer
};

const int TRIGGER_REMOVE_FIELDS_COUNT = 1;
const int TRIGGER_REMOVE_FIELDS[] = {
    4, // trigger identififer
};

const int SENSOR_ACTIVATE_FIELDS_COUNT = 1;
const int SENSOR_ACTIVATE_FIELDS[] = {
    1, // sensor interface
};

const int SENSOR_DEACTIVATE_FIELDS_COUNT = 1;
const int SENSOR_DEACTIVATE_FIELDS[] = {
    1, // sensor interface
};

const int ACTUATOR_ACTIVATE_FIELDS_COUNT = 1;
const int ACTUATOR_ACTIVATE_FIELDS[] = {
    1, // actuator interface
};

const int ACTUATOR_DEACTIVATE_FIELDS_COUNT = 1;
const int ACTUATOR_DEACTIVATE_FIELDS[] = {
    1, // actuator interface
};


int extract_packet_body
(const char* packet, size_t packet_len, const int* fields, int fields_count, Packet** output);



Packet* protocol_parse_packet (char* packet, size_t packet_len) {
    if (packet_len < HEADER_LENGTH)
        return NULL;

    Packet* result = malloc(sizeof(Packet));

    int use_crypto = conf_get(USE_CRYPTO);
    if (use_crypto == 1) {
        char* mac   = &(packet[packet_len - CRYPTO_MAC_LENGTH - CRYPTO_NONCE_LENGTH]);
        char* nonce = &(packet[packet_len - CRYPTO_NONCE_LENGTH]);
        crypto_decipher(packet, packet_len, mac, nonce);
    }

    result->mote_id   = 0;
    result->mote_id  += (0xff & packet[0]) << 8;
    result->mote_id  += (0xff & packet[1]);
    result->type      = (PacketType) (0xff & packet[3]);
    result->number    = (0xff & packet[2]);
    result->length    = 0;
    result->length   += (0xff & packet[4]) << 8;
    result->length   += (0xff & packet[5]);
    result->nr_fields = 0;
    result->fields    = NULL;

    const int* fields;
    int fields_count;

    switch (result->type) {
    case ACK:
        fields = ACK_FIELDS;
        fields_count = ACK_FIELDS_COUNT;
        break;

    case NACK:
        fields = ACK_FIELDS;
        fields_count = ACK_FIELDS_COUNT;
        break;

    case CONF_SET:
        fields = CONF_SET_FIELDS;
        fields_count = CONF_SET_FIELDS_COUNT;
        break;

    case REQUEST_MEASUREMENTS:
        fields = REQUEST_MEASUREMENTS_FIELDS;
        fields_count = REQUEST_MEASUREMENTS_FIELDS_COUNT;
        break;

    case REQUEST_STATUS_REPORT:
        fields = REQUEST_STATUS_REPORT_FIELDS;
        fields_count = REQUEST_STATUS_REPORT_FIELDS_COUNT;
        break;

    case ALERT_SET:
        fields = ALERT_SET_FIELDS;
        fields_count = ALERT_SET_FIELDS_COUNT;
        break;

    case ALERT_REMOVE:
        fields = ALERT_REMOVE_FIELDS;
        fields_count = ALERT_REMOVE_FIELDS_COUNT;
        break;

    case TRIGGER_SET:
        fields = TRIGGER_SET_FIELDS;
        fields_count = TRIGGER_SET_FIELDS_COUNT;
        break;

    case TRIGGER_REMOVE:
        fields = TRIGGER_REMOVE_FIELDS;
        fields_count = TRIGGER_REMOVE_FIELDS_COUNT;
        break;

    case SENSOR_ACTIVATE:
        fields = SENSOR_ACTIVATE_FIELDS;
        fields_count = SENSOR_ACTIVATE_FIELDS_COUNT;
        break;

    case SENSOR_DEACTIVATE:
        fields = SENSOR_DEACTIVATE_FIELDS;
        fields_count = SENSOR_DEACTIVATE_FIELDS_COUNT;
        break;

    case ACTUATOR_ACTIVATE:
        fields = ACTUATOR_ACTIVATE_FIELDS;
        fields_count = ACTUATOR_ACTIVATE_FIELDS_COUNT;
        break;

    case ACTUATOR_DEACTIVATE:
        fields = ACTUATOR_DEACTIVATE_FIELDS;
        fields_count = ACTUATOR_DEACTIVATE_FIELDS_COUNT;
        break;

    default:
        protocol_free_packet(result);
        return NULL;
    }

    if (extract_packet_body(packet, packet_len, fields, fields_count, &result) == -1) {
        protocol_free_packet(result);
        return NULL;
    }

    return result;
}



size_t protocol_make_packet (Packet* packet, char** output) {
    size_t output_size = HEADER_LENGTH;
    int i;
    for (i = 0; i < packet->nr_fields; i++) {
        output_size += packet->fields[i].byte_len;
    }

    size_t payload_len = output_size - HEADER_LENGTH;
    int use_crypto = conf_get(USE_CRYPTO);
    if (use_crypto == 1) {
        // space for the mac and the nonce at the end of the packet
        output_size += (CRYPTO_MAC_LENGTH + CRYPTO_NONCE_LENGTH);
    }

    *output = malloc(output_size);
    memset(*output, 0, output_size);

    (*output)[0] = (0xff & (packet->mote_id >> 8));
    (*output)[1] = (0xff & packet->mote_id);
    (*output)[2] = (0xff & packet->number);
    (*output)[3] = (0xff & packet->type);
    (*output)[4] = (0xff & (payload_len >> 8));
    (*output)[5] = (0xff & payload_len);

    int position = HEADER_LENGTH;
    for (i = 0; i < packet->nr_fields; i++) {
        int nr_bytes = packet->fields[i].byte_len;
        int b;
        for (b = 0; b < nr_bytes; b++) {
            (*output)[position + (nr_bytes - b - 1)] =
                 0xff & (packet->fields[i].value >> (b*8));
        }
        position += nr_bytes;
    }

    if (use_crypto == 1) {
        char* mac   = &((*output)[output_size - CRYPTO_MAC_LENGTH - CRYPTO_NONCE_LENGTH]);
        char* nonce = &((*output)[output_size - CRYPTO_NONCE_LENGTH]);
        crypto_cipher((*output), output_size- CRYPTO_MAC_LENGTH - CRYPTO_NONCE_LENGTH, mac, nonce);
    }

    return output_size;
}



int extract_packet_body
(const char* packet, size_t packet_len, const int* fields, int fields_count, Packet** output) {


    if (fields_count == 0) {
        (*output)->nr_fields = 0;
        return 0;
    }

    (*output)->fields    = malloc(fields_count * sizeof(PacketField));
    (*output)->nr_fields = fields_count;

    int position = HEADER_LENGTH;
    int i;
    for (i = 0; i < fields_count; i++) {
        int nr_bytes = fields[i];

        if (packet_len < position + nr_bytes) {
            return -1;
        }

        field_t field_value = 0;
        int b;
        for (b = 0; b < nr_bytes; b++) {
            field_value += ((0xff & packet[position + b]) << (8 * (nr_bytes - b - 1)));
        }

        (*output)->fields[i].value    = field_value;
        (*output)->fields[i].byte_len = nr_bytes;
        position += nr_bytes;
    }

    return 0;
}

