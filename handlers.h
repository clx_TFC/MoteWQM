#ifndef __OURICO_WQM_HANDLERS_H__
#define __OURICO_WQM_HANDLERS_H__


#include <time.h>

#include "protocol.h"
#include "gprs_comm.h"
#include "sensors.h"
#include "alerts.h"
#include "config.h"
#include "stats.h"
#include "util.h"


void handle_packet (Packet* packet, const char* raw_packet, size_t packet_len);


/**
 * Send last measurements to server
 *
 * @param packet_no number to use in packet
 *
 * */
void handle_send_measurements (int packet_no);


/**
 * Send status report to server
 *
 * @param packet_no number to use in packet
 *
 * */
void handle_send_status_report (int packet_no);



/**
 * Send an alert to server
 *
 * @param alert_id  id of alert to send
 * @param meas      measurement generating the alert
 * @param nr_meas   nr of measurements in meas list
 * @param packet_no number to use in packet
 *
 * */
void handle_send_alert (int alert_id, Measurement* meas, int nr_mreas, int packet_no);


#endif // __OURICO_WQM_HANDLERS_H__
