#include "handlers.h"


void handle_packet (Packet* packet, const char* raw_packet, size_t packet_len) {

    switch (packet->type) {
    case SENSOR_ACTIVATE:
        sensors_activate(packet->fields[0].value);
        break;

    case SENSOR_DEACTIVATE:
        sensors_deactivate(packet->fields[0].value);
        break;

    case ALERT_SET:
        alerts_activate(packet->fields[0].value);
        break;

    case ALERT_REMOVE:
        alerts_deactivate(packet->fields[0].value);
        break;

    case REQUEST_MEASUREMENTS:
        handle_send_measurements(packet->number);
        return;

    case REQUEST_STATUS_REPORT:
        handle_send_status_report(packet->number);
        return;

    case CONF_SET:
        conf_set(MEASUREMENT_PERIOD, packet->fields[0].value);
        conf_set(SEND_DATA_PERIOD, packet->fields[1].value);
        conf_set(STATUS_REPORT_PERIOD, packet->fields[2].value);
        conf_set(USE_CRYPTO, packet->fields[3].value);
        break;

    default:
        util_nack(packet);
        return;
    }

    if (!gprs_comm_send(raw_packet, packet_len))
        stats_lost(packet_len);
    else
        stats_tx(packet_len);
}


void handle_send_measurements (int packet_no) {
    Measurement* measurements;
    int nr_of_meas = sensors_get_measurements(&measurements);
    if (nr_of_meas < 0 || measurements == NULL) {
        util_err(FAILED_TO_MEASURE);
        return;
    } else if (nr_of_meas == 0) {
        sensors_free_measurements(measurements, nr_of_meas);
        return;
    }

    int nr_fields = MEASUREMENTS_FIELDS_COUNT;
    int i;
    for (i = 0; i < nr_of_meas; i++) {
        nr_fields += measurements[i].nr_parameters + 1;
    }

    Packet* packet = protocol_alocate_packet(
        CONF_THIS_MOTE_ID,
        MEASUREMENTS,
        packet_no,
        nr_fields
    );

    // flags
    packet->fields[0].value    = 0;
    packet->fields[0].byte_len = MEASUREMENTS_FIELDS[0];
    // date and time
    packet->fields[1].value    = util_mk_time(time(NULL));
    packet->fields[1].byte_len = MEASUREMENTS_FIELDS[1];

    int k, field_idx = 2;
    for (i = 0; i < nr_of_meas; i++) {
        packet->fields[field_idx].value    = measurements[i].interface_num;
        packet->fields[field_idx].byte_len = PROTOCOL_INTERFACE_NUM_SIZE;
        field_idx++;
        for (k = 0; k < measurements[i].nr_parameters; k++) {
            packet->fields[field_idx].value    = measurements[i].parameter_values[i];
            packet->fields[field_idx].byte_len = 4;
            field_idx++;
        }
    }

    char* packet_bytes = NULL;
    size_t packet_len = protocol_make_packet(packet, &packet_bytes);

    if (packet_bytes != NULL) {
        if (!gprs_comm_send(packet_bytes, packet_len))
            stats_lost(packet_len);
        else
            stats_tx(packet_len);

        free(packet_bytes);
    }

    protocol_free_packet(packet);
    sensors_free_measurements(measurements, nr_of_meas);
}


void handle_send_status_report (int packet_no) {
    Stats* stats = stats_get();

    Packet* packet = protocol_alocate_packet(
        CONF_THIS_MOTE_ID,
        STATUS_REPORT,
        packet_no,
        STATUS_REPORT_FIELDS_COUNT
    );

    packet->fields[0].value = util_mk_time(stats->start);
    packet->fields[0].byte_len = STATUS_REPORT_FIELDS[0];
    packet->fields[1].value = util_mk_time(stats->end);
    packet->fields[1].byte_len = STATUS_REPORT_FIELDS[1];
    packet->fields[2].value = stats->nr_tx_packets;
    packet->fields[2].byte_len = STATUS_REPORT_FIELDS[2];
    packet->fields[3].value = stats->nr_lost_packets;
    packet->fields[3].byte_len = STATUS_REPORT_FIELDS[3];
    packet->fields[4].value = stats->nr_tx_bytes;
    packet->fields[4].byte_len = STATUS_REPORT_FIELDS[4];
    packet->fields[5].value = stats->nr_lost_bytes;
    packet->fields[5].byte_len = STATUS_REPORT_FIELDS[5];

    char* packet_bytes = NULL;
    size_t packet_len = protocol_make_packet(packet, &packet_bytes);

    if (packet_bytes != NULL) {
        if (!gprs_comm_send(packet_bytes, packet_len))
            stats_lost(packet_len);
        else
            stats_tx(packet_len);

        free(packet_bytes);
    }

    protocol_free_packet(packet);
    stats_free(stats);
}


void handle_send_alert (int alert_id, Measurement* meas, int nr_meas, int packet_no) {

    int nr_fields = ALERT_FIELDS_COUNT;
    int i;
    for (i = 0; i < nr_meas; i++) {
        nr_fields += meas[i].nr_parameters + 1;
    }

    Packet* packet = protocol_alocate_packet(
        CONF_THIS_MOTE_ID,
        ALERT,
        packet_no,
        nr_fields
    );

    // date and time
    packet->fields[0].value    = util_mk_time(time(NULL));
    packet->fields[0].byte_len = ALERT_FIELDS[0];
    // alert id
    packet->fields[1].value    = alert_id;
    packet->fields[1].byte_len = ALERT_FIELDS[1];

    // add measurements
    int k, field_idx = 2;
    for (i = 0; i < nr_meas; i++) {
        packet->fields[field_idx].value    = meas[i].interface_num;
        packet->fields[field_idx].byte_len = PROTOCOL_INTERFACE_NUM_SIZE;
        field_idx++;
        for (k = 0; k < meas[i].nr_parameters; k++) {
            packet->fields[field_idx].value    = meas[i].parameter_values[i];
            packet->fields[field_idx].byte_len = 4;
            field_idx++;
        }
    }

    char* packet_bytes = NULL;
    size_t packet_len = protocol_make_packet(packet, &packet_bytes);

    if (packet_bytes != NULL) {
        if (!gprs_comm_send(packet_bytes, packet_len))
            stats_lost(packet_len);
        else
            stats_tx(packet_len);

        free(packet_bytes);
    }

    protocol_free_packet(packet);

}

