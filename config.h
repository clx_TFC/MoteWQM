#ifndef __OURICO_WQM_CONFIG_H__
#define __OURICO_WQM_CONFIG_H__

/**
 * Global constant values used in the program
 *
 * */
#define CONF_SERVER_ADDRESS "serveo.net"
#define CONF_SERVER_PORT    30000
#define CONF_THIS_MOTE_ID   1
// "the_secret_key!!"
#define CONF_CRYPTO_KEY     { 116, 104, 101, 95, 115, 101, 99, 114, 101, 116, 95, 107, 101, 121, 33, 33 }

#define CONF_RESERVOIR_CAPACITY 4 // meters


/**
 * Configurations that may change at runtime
 *
 * */
typedef enum {

    MEASUREMENT_PERIOD = 0,
    SEND_DATA_PERIOD,
    STATUS_REPORT_PERIOD,
    USE_CRYPTO

} Configuration;


#define __CONF_MEASUREMENT_PERIOD_DEFAULT   18 // seconds
#define __CONF_SEND_DATA_PERIOD_DEFAULT     36 // seconds
#define __CONF_STATUS_REPORT_PERIOD_DEFAULT 72 // seconds
#define __CONF_USE_CRYPTO_DEFAULT           0    // false


/**
 * Get current value of a parameter
 *
 * @param conf the parameter to get
 *
 * @return the current value
 *
 * */
int conf_get (Configuration conf);


/**
 * Change a parameters' value
 *
 * @param conf  the parameter to set
 * @param value the new value
 *
 * */
void conf_set (Configuration conf, int value);



#endif // __OURICO_WQM_CONFIG_H__














