#ifndef __OURICO_WQM_STATS_H__
#define __OURICO_WQM_STATS_H__

#include <stdlib.h>
#include <time.h>


typedef struct {

    time_t start;
    time_t end;
    int nr_tx_packets;
    int nr_lost_packets;
    int nr_tx_bytes;
    int nr_lost_bytes;

} Stats;



void stats_init ();


/**
 * Increment the number of transferred packets
 *
 * @param bytes how many bytes were sent
 *
 * */
void stats_tx (int bytes);


/**
 * Increment the number of lost packets
 *
 * @param bytes size of the failed packet
 *
 * */
void stats_lost (int bytes);



/**
 * Get last statistics
 *
 * */
Stats* stats_get ();



inline void stats_free (Stats* s) {
    if (s != NULL)
        free(s);
}



#endif // __OURICO_WQM_STATS_H__
