#ifndef __OURICO_WQM_CRYPTO_H__
#define __OURICO_WQM_CRYPTO_H__


#include <ti/drivers/crypto/CryptoCC26XX.h>
#include <xdc/std.h>
#include <xdc/runtime/System.h>

#include <stdlib.h>
#include <time.h>

#include "CC1310DK_7XD.h"
#include "Board.h"

#include "config.h"
#include "protocol.h"




#define CRYPTO_MAC_LENGTH    4  // bytes
#define CRYPTO_NONCE_LENGTH  12 // bytes
#define CRYPTO_FIELD_LENGTH  3  // corresponds to nonce length = 12
#define CRYPTO_HEADER_LENGTH 2  // bytes
#define CRYPTO_KEY_LEN       16


void crypto_init ();


int crypto_cipher (char* packet, size_t packet_len, char* mac, char* nonce);


int crypto_decipher (char *packet, size_t packet_len, char* mac, char* nonce);




#endif // __OURICO_WQM_CRYPTO_H__














