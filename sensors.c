#include "sensors.h"


static bool __wl_sensor_active = true;
static bool __wq_sensor_active = true;


static float __wl_mean                 = 0;
static float __wl_nr_samples           = 0;
static float __salinity_mean           = 0;
static float __salinity_nr_samples     = 0;
static float __conductivity_mean       = 0;
static float __conductivity_nr_samples = 0;
static float __tds_mean                = 0;
static float __tds_nr_samples          = 0;



void sensors_do_measure () {

    srand(time(NULL));
    float __wl_reading           = 1.4; // rand() % CONF_RESERVOIR_CAPACITY;
    float __tds_reading          = rand() % 100; // 0.12;
    float __conductivity_reading = rand() % 100; // 0.12;
    float __salinity_reading     = rand() % 100; // 0.12;

    __wl_nr_samples++;
    __tds_nr_samples++;
    __conductivity_nr_samples++;
    __salinity_nr_samples++;

    __wl_mean = (__wl_reading - __wl_mean)/__wl_nr_samples + __wl_mean;
    __tds_mean = (__tds_reading - __tds_mean)/__tds_nr_samples + __tds_mean;
    __conductivity_mean =
            (__conductivity_reading - __conductivity_mean)/__conductivity_nr_samples  +
            __conductivity_mean;
    __salinity_mean =
            (__salinity_reading - __salinity_mean)/__salinity_nr_samples +
            __salinity_mean;

    // check alerts
    bool with_alert = false;
    int alert_id    = 0;
    if (alert_is_active(XLOW_WL_ALERT_ID)) {
        if (__wl_reading < XLOW_WL_THRESHOLD*CONF_RESERVOIR_CAPACITY) {
            with_alert = true;
            alert_id = XLOW_WL_ALERT_ID;
        }
    } else if (alert_is_active(LOW_WL_ALERT_ID)) {
        if (__wl_reading < LOW_WL_THRESHOLD*CONF_RESERVOIR_CAPACITY) {
            with_alert = true;
            alert_id = LOW_WL_ALERT_ID;
        }
    }

    if (with_alert) {
        Measurement* measurements;
        measurements = malloc(2 * sizeof(Measurement));
        measurements[0].interface_num = WL_SENSOR_INTERFACE_NUM;
        measurements[1].interface_num = WQ_SENSOR_INTERFACE_NUM;

        measurements[0].nr_parameters       = 1;
        measurements[0].parameter_values    = malloc(sizeof(int));
        measurements[0].parameter_values[0] = (__wl_reading)*1000;

        measurements[1].nr_parameters       = 3;
        measurements[1].parameter_values    = malloc(3 * sizeof(int));
        measurements[1].parameter_values[0] = (__salinity_reading)*1000;
        measurements[1].parameter_values[1] = (__conductivity_reading)*1000;
        measurements[1].parameter_values[2] = (__tds_reading)*1000;

        handle_send_alert(alert_id, measurements, 2, 0);

        sensors_free_measurements(measurements, 2);
    }
}



int sensors_get_measurements (Measurement** measurements) {

    __conductivity_nr_samples = 0;
    __wl_nr_samples           = 0;
    __salinity_nr_samples     = 0;
    __tds_nr_samples          = 0;

    if (__wl_sensor_active && __wq_sensor_active) {

        (*measurements) = malloc(2 * sizeof(Measurement));
        (*measurements)[0].interface_num = WL_SENSOR_INTERFACE_NUM;
        (*measurements)[1].interface_num = WQ_SENSOR_INTERFACE_NUM;

        (*measurements)[0].nr_parameters       = 1;
        (*measurements)[0].parameter_values    = malloc(sizeof(int));
        (*measurements)[0].parameter_values[0] = (__wl_mean)*1000;

        (*measurements)[1].nr_parameters       = 3;
        (*measurements)[1].parameter_values    = malloc(3 * sizeof(int));
        (*measurements)[1].parameter_values[0] = (__salinity_mean)*1000;
        (*measurements)[1].parameter_values[1] = (__conductivity_mean)*1000;
        (*measurements)[1].parameter_values[2] = (__tds_mean)*1000;

        return 2;

    } else if (__wl_sensor_active) {

        (*measurements) = malloc(sizeof(Measurement));
        (*measurements)[0].interface_num = WL_SENSOR_INTERFACE_NUM;
        (*measurements)[0].nr_parameters = 1;
        (*measurements)[0].parameter_values = malloc(sizeof(int));
        (*measurements)[0].parameter_values[0] = (__wl_mean)*1000;

        return 1;

    } else if (__wq_sensor_active) {

        (*measurements) = malloc(sizeof(Measurement));
        (*measurements)[0].interface_num = WQ_SENSOR_INTERFACE_NUM;
        (*measurements)[0].nr_parameters = 3;
        (*measurements)[0].parameter_values = malloc(3 * sizeof(int));
        (*measurements)[0].parameter_values[0] = (__salinity_mean)*1000;
        (*measurements)[0].parameter_values[1] = (__conductivity_mean)*1000;
        (*measurements)[0].parameter_values[2] = (__tds_mean)*1000;

        return 1;

    }

    return 0;
}


void sensors_activate (int interface_num) {
    switch (interface_num) {

    case WL_SENSOR_INTERFACE_NUM:
        __wl_sensor_active = false;
        break;
    case WQ_SENSOR_INTERFACE_NUM:
        __wq_sensor_active = false;
        break;

    }
}


void sensors_deactivate (int interface_num) {
    switch (interface_num) {

    case WL_SENSOR_INTERFACE_NUM:
        __wl_sensor_active = true;
        break;
    case WQ_SENSOR_INTERFACE_NUM:
        __wq_sensor_active = true;
        break;

    }
}
