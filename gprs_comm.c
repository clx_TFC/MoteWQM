#include "gprs_comm.h"
#include "stdbool.h"


static UART_Handle _uart;
static bool is_online = false;


void gprs_comm_init () {

    UART_Params uartParams;
    UART_Params_init(&uartParams);
    uartParams.readReturnMode = UART_RETURN_NEWLINE;
    uartParams.readEcho       = UART_ECHO_OFF;
    uartParams.baudRate       = 115200;
    _uart = UART_open(Board_UART0, &uartParams);
    if (_uart == NULL) {
        System_abort("[gprs_comm_init] Error opening UART");
    }
    UART_control(_uart, UARTCC26XX_CMD_RETURN_PARTIAL_ENABLE, NULL);

    const int tmp_sz = 100;
    char *tmp = malloc(tmp_sz);

    util_uart_write_and_read(
        _uart, AT_AT, strlen(AT_AT), tmp, tmp_sz
    );
    util_uart_write_and_read(
        _uart, AT_DISABLE_ECHO, strlen(AT_DISABLE_ECHO), tmp, tmp_sz
    );
    util_uart_write_and_read(
        _uart,
        AT_ENABLE_VERBOSE_ERRORS,
        strlen(AT_ENABLE_VERBOSE_ERRORS),
        tmp, tmp_sz
    );

    // set up gprs
    util_uart_write_and_read(
        _uart, AT_GPRS_ATTACH, strlen(AT_GPRS_ATTACH), tmp, tmp_sz
    );
    util_uart_write_and_read(
        _uart,
        AT_ADD_PDP_CONTEXT,
        strlen(AT_ADD_PDP_CONTEXT),
        tmp, tmp_sz
    );
    util_uart_write_and_read(
        _uart,
        AT_ACTIVATE_PDP_CONTEXT,
        strlen(AT_ACTIVATE_PDP_CONTEXT), tmp, tmp_sz
    );

    free(tmp);
}



bool gprs_comm_connect (const char *server_addr, int server_port) {
    int len   = strlen(server_addr) + 30;
    char* conn = malloc(len);
    snprintf(conn, len, "AT#SD=3,0,%d,\"%s\",0,0,0\r", server_port, server_addr);

    const int result_sz = 100;
    char* result = malloc(result_sz);

    util_uart_write_and_read(
        _uart,
        AT_SOCKET_CONFIG,
        strlen(AT_SOCKET_CONFIG), result, result_sz
    );
    util_uart_write_and_read(
        _uart,
        AT_SOCKET_CONFIG_EXT,
        strlen(AT_SOCKET_CONFIG_EXT), result, result_sz
    );
    memset(result, 0, result_sz);
    util_uart_write_and_read(_uart, conn, strlen(conn), result, result_sz);

    free(conn);
    is_online = strstr(result, "CONNECT") != NULL;
    free(result);

    return is_online;
}



bool gprs_comm_send (const char* packet, size_t packet_size) {

    /*const int result_sz = 100;
    char* result = malloc(result_sz);
    memset(result, 0, result_sz);

    char* send = malloc(30);
    snprintf(send, 30, "AT#SSENDEXT=3,%u\r", packet_size);
    util_uart_write_and_read(_uart, send, strlen(send), result, result_sz);
    memset(result, 0, result_sz);

    util_uart_write_and_read(_uart, packet, packet_size, result, result_sz);
    bool sent = strstr(result, "OK") != NULL;

    free(send);
    free(result);*/

    int sent = UART_write(_uart, packet, packet_size);
    Task_sleep(MODULE_LAG);
    return sent != UART_ERROR;
}



int gprs_comm_recv (char* buffer, size_t buffer_size) {
    //char* result = malloc(MAX_IN_BUFFER_SZ);
    //memset(result, 0, MAX_IN_BUFFER_SZ);
    //read = util_uart_write_and_read(
    //    _uart, AT_READ_DATA, strlen(AT_READ_DATA), buffer, buffer_size
    //);

    // expected #SRECV: 3,<received bytes>
    /*if (strstr(result, "#SRECV: 3") == NULL) {
        System_printf("%s", result);
        System_flush();
        free(result);
        return -1;
    }*/

    /*strtok(result, ",");
    char* recvd = strtok("\r");*/

    //free(result);

    int read = UART_read(_uart, buffer, buffer_size);
    if (read == UART_ERROR) return -1;
    return read;
}



IP gprs_comm_get_assigned_ip () {
    const int result_sz = 100;
    char* result = malloc(result_sz);
    memset(result, 0, result_sz);

    util_uart_write_and_read(
        _uart, AT_GET_ASSIGNED_IP, strlen(AT_GET_ASSIGNED_IP), result, result_sz
    );

    char* ip_start  = strchr(result, '\"');
    char* _byte_1   = strtok(ip_start+1, ".");
    char* _byte_2   = strtok(NULL, ".");
    char* _byte_3   = strtok(NULL, ".");
    char* _byte_4   = strtok(NULL, ".");
    _byte_4         = strtok(_byte_4, "\"");

    IP ip;
    ip.a = atoi(_byte_1);
    ip.b = atoi(_byte_2);
    ip.c = atoi(_byte_3);
    ip.d = atoi(_byte_4);

    free(result);
    return ip;
}



bool gprs_comm_is_connected () {
    const int result_sz = 100;
    char* result = malloc(result_sz);
    memset(result, 0, result_sz);

    if (is_online) {
        /*Task_sleep(MODULE_LAG);
        util_uart_write_and_read(
            _uart, AT_PAUSE_CONNECTION, strlen(AT_PAUSE_CONNECTION), result, result_sz
        );
        Task_sleep(MODULE_LAG);*/
    }

    util_uart_write_and_read(
        _uart, AT_IS_CONNECTED, strlen(AT_IS_CONNECTED), result, result_sz
    );
    bool is_conn = strstr(result, "#SS: 3,0") == NULL;
    if (is_conn) {
       /* util_uart_write_and_read(
            _uart, AT_RESUME_CONNECTION, strlen(AT_RESUME_CONNECTION), result, result_sz
        );
        Task_sleep(MODULE_LAG);*/
    } else {
        is_online = false;
    }

    free(result);
    return is_conn;
}









