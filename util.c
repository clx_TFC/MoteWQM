#include "util.h"


void util_ack (Packet* packet) {

}


void util_nack (Packet* packet) {

}


void util_err (ErrorCode err) {

}


uint64_t util_mk_time (time_t t) {
    struct tm* _t = localtime(&t);

    uint64_t byte_1 = 0xff & (_t->tm_year - 100); // tm_year here gives years since 1900
                                                 // we want years since 2000
    uint64_t byte_2 = 0xff & (_t->tm_mon & 0x0f) + ((_t->tm_mday << 4) & 0xf0);
    uint64_t byte_3 = 0xff & ((_t->tm_mday >> 4) & 0x01) + (_t->tm_hour & 0x3e);
    uint64_t byte_4 = 0xff & (_t->tm_min & 0x3f);
    uint64_t byte_5 = 0xff & (_t->tm_sec & 0x3f);

    System_printf(
        "Data: %d-%d-%d %d-%d-%d",
        _t->tm_hour,
        _t->tm_min,
        _t->tm_sec,
        _t->tm_mday,
        _t->tm_mon,
        byte_1
    ); System_flush();

    uint64_t result = 0;
    result += (byte_1 << 32L);
    result += (byte_2 << 24);
    result += (byte_3 << 16);
    result += (byte_4 << 8);
    result += (byte_5);

    return result;
}
