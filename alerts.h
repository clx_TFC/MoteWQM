#ifndef __OURICO_WQM_ALERTS_H__
#define __OURICO_WQM_ALERTS_H__

#include <stdbool.h>


#define LOW_WL_ALERT_ID  1
#define XLOW_WL_ALERT_ID 2

#define LOW_WL_THRESHOLD  0.5
#define XLOW_WL_THRESHOLD 0.25



/**
 * Activate an alert
 *
 * @param alert_id id of the alert to activate
 *
 * */
void alerts_activate (int alert_id);


/**
 * Deactivate an alert
 *
 * @param alert_id id of the alert to deactivate
 *
 * */
void alerts_deactivate (int alert_id);



/**
 * Check if alert is active
 *
 * @param alert_id id of alert to check
 *
 * @return true if active
 *
 * */
bool alert_is_active (int alert_id);



#endif // __OURICO_WQM_ALERTS_H__
