#include "config.h"


static int _conf_values[] = {
   __CONF_MEASUREMENT_PERIOD_DEFAULT,
   __CONF_SEND_DATA_PERIOD_DEFAULT,
   __CONF_STATUS_REPORT_PERIOD_DEFAULT,
   __CONF_USE_CRYPTO_DEFAULT
};


int conf_get (Configuration conf) {
    return _conf_values[(int) conf];
}

void conf_set (Configuration conf, int value) {
    _conf_values[(int) conf] = value;
}
