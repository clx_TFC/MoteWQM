#include "crypto.h"


static CryptoCC26XX_Handle _crypto;

void crypto_rand_nonce (char* nonce, size_t nonce_len);



void crypto_init () {
    CryptoCC26XX_Params params;
    CryptoCC26XX_Params_init(&params);
    _crypto = CryptoCC26XX_open(Board_CRYPTO, false, &params);
    if (_crypto == NULL) {
        System_abort("[crypto_init] Error opening Crypto");
    }
}


int crypto_cipher (char* packet, size_t packet_len, char* mac, char* nonce) {

    if (packet_len < CRYPTO_HEADER_LENGTH)
        return -1;

    CryptoCC26XX_AESCCM_Transaction transaction;
    CryptoCC26XX_Transac_init(
        (CryptoCC26XX_Transaction*) &transaction,
        CRYPTOCC26XX_OP_AES_CCM_ENCRYPT
    );

    uint8_t key[CRYPTO_KEY_LEN] = CONF_CRYPTO_KEY;
    int key_idx = CryptoCC26XX_allocateKey(
        _crypto, CRYPTOCC26XX_KEY_ANY, (const uint32_t*) key
    );
    if (key_idx == CRYPTOCC26XX_STATUS_ERROR)
        return -1;

    crypto_rand_nonce(nonce, CRYPTO_NONCE_LENGTH);

    transaction.keyIndex     = key_idx;
    transaction.authLength   = CRYPTO_MAC_LENGTH;
    transaction.nonce        = nonce;
    // header is located in the beginning of the packet
    transaction.header       = packet;
    transaction.fieldLength  = CRYPTO_FIELD_LENGTH;
    transaction.msgInLength  = packet_len - CRYPTO_HEADER_LENGTH;
    transaction.headerLength = CRYPTO_HEADER_LENGTH;
    // after the header comes the content to encrypt
    transaction.msgIn        = &(packet[CRYPTO_HEADER_LENGTH]);
    transaction.msgOut       = mac;

    int32_t crypto_result =
        CryptoCC26XX_transact(_crypto, (CryptoCC26XX_Transaction*) &transaction);
    if (crypto_result != CRYPTOCC26XX_STATUS_SUCCESS) {
        return -1;
    }

    crypto_result = CryptoCC26XX_releaseKey(_crypto, &key_idx);
    if (crypto_result != CRYPTOCC26XX_STATUS_SUCCESS) {
        System_abort("[crypto_cipher] failed to release key");
    }

    return 0;
}


int crypto_decipher (char* packet, size_t packet_len, char* mac, char* nonce) {

    CryptoCC26XX_AESCCM_Transaction transaction;
    CryptoCC26XX_Transac_init(
        (CryptoCC26XX_Transaction*) &transaction,
        CRYPTOCC26XX_OP_AES_CCM_DECRYPT
    );

    uint8_t key[CRYPTO_KEY_LEN] = CONF_CRYPTO_KEY;
    int key_idx = CryptoCC26XX_allocateKey(
        _crypto, CRYPTOCC26XX_KEY_ANY, (const uint32_t*) key
    );
    if (key_idx == CRYPTOCC26XX_STATUS_ERROR)
        return -1;

    transaction.keyIndex     = key_idx;
    transaction.authLength   = CRYPTO_MAC_LENGTH;
    transaction.nonce        = nonce;
    // header is located in the beginning of the packet
    transaction.header       = packet;
    transaction.fieldLength  = CRYPTO_FIELD_LENGTH;
    transaction.msgInLength  = packet_len - CRYPTO_HEADER_LENGTH;
    transaction.headerLength = CRYPTO_HEADER_LENGTH;
    // after the header comes the content to decrypt
    transaction.msgIn        = &(packet[CRYPTO_HEADER_LENGTH]);
    transaction.msgOut       = mac;

    int32_t crypto_result =
        CryptoCC26XX_transact(_crypto, (CryptoCC26XX_Transaction*) &transaction);
    if (crypto_result != CRYPTOCC26XX_STATUS_SUCCESS) {
        return -1;
    }

    crypto_result = CryptoCC26XX_releaseKey(_crypto, &key_idx);
    if (crypto_result != CRYPTOCC26XX_STATUS_SUCCESS) {
        System_abort("[crypto_decipher] failed to release key");
    }

    return 0;
}


void crypto_rand_nonce (char* nonce, size_t nonce_len) {
    int i;
    srand(time(NULL));
    for (i = 0; i < nonce_len; i++) {
        nonce[i] = 33 + 93 * (rand() / (RAND_MAX));
    }
}




















