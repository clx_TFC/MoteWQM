#include "stats.h"


static int __start           = 0;
static int __nr_tx_packets   = 0;
static int __nr_tx_bytes     = 0;
static int __nr_lost_packets = 0;
static int __nr_lost_bytes   = 0;



void stats_init () {
    __start = time(NULL);
}


void stats_tx (int bytes) {
    __nr_tx_packets++;
    __nr_tx_bytes += bytes;
}


void stats_lost (int bytes) {
    __nr_lost_packets++;
    __nr_lost_bytes += bytes;
}



/**
 * Get last statistics
 *
 * */
Stats* stats_get () {
    Stats* s = malloc(sizeof(Stats));

    s->nr_tx_packets   = __nr_tx_packets;
    s->nr_lost_packets = __nr_lost_packets;
    s->nr_tx_bytes     = __nr_tx_bytes;
    s->nr_lost_bytes   = __nr_lost_bytes;
    s->end             = time(NULL);
    s->start           = __start;

    __nr_tx_packets   = 0;
    __nr_lost_packets = 0;
    __nr_tx_packets   = 0;
    __nr_lost_bytes   = 0;
    __start = s->end;

    return s;
}
