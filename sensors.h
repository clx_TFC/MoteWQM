#ifndef __OURICO_WQM_SENSOR_H__
#define __OURICO_WQM_SENSOR_H__

#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#include "alerts.h"
#include "config.h"


#define WL_SENSOR_INTERFACE_NUM 0 // water level
#define WQ_SENSOR_INTERFACE_NUM 1 // water quality


typedef struct {

    int interface_num;
    size_t nr_parameters;
    int* parameter_values;

} Measurement;

#include "handlers.h"


/**
 * Get the last measurements from the sensors
 *
 * */
void sensors_do_measure ();


/**
 * Return the cached measurements from the sensors
 *
 * @param measurements used to return the measurements (will be allocated)
 *
 * @return the number of measurements inserted in `measurements`
 * */
int sensors_get_measurements (Measurement** measurements);


/**
 * Activate a sensor
 *
 * @param interface_num interface where the sensor is intalled
 * */
void sensors_activate (int interface_num);


/**
 * Deactivate a sensor
 *
 * @param interface_num interface where the sensor is intalled
 * */
void sensors_deactivate (int interface_num);


/**
 * Free a list of measurements
 *
 * @param m          the list of measurements
 * @param nr_of_meas nr of items in `m`
 * */
inline void sensors_free_measurements (Measurement* m, int nr_of_meas) {
    int i = 0;
    if (m != NULL) {
        for (; i < nr_of_meas; i++)
            if (m[i].parameter_values != NULL)
                free(m[i].parameter_values);
        free(m);
    }
}


#endif // __OURICO_WQM_SENSOR_H__












