#ifndef __OURICO_WQM_UTIL_H__
#define __OURICO_WQM_UTIL_H__

#include <ti/drivers/UART.h>
#include <ti/drivers/uart/UARTCC26XX.h>

#include <time.h>

#include "protocol.h"


typedef enum {

    FAILED_TO_MEASURE         = 0,
    FAILED_TO_EXECUTE_ACTION  = 1,
    RECEIVED_MALFORMED_PACKET = 2,

} ErrorCode;


/**
 * Write to UART and then read
 *
 * @param uart     handle
 * @param msg      data to write
 * @param msg_size size of `msg` array
 * @param out      buffer to receive read result
 * @param out_size size of `out` array
 *
 * @return number of read bytes
 * */
inline int util_uart_write_and_read (
    UART_Handle uart,
    const void *msg, size_t msg_size,
    void *out, size_t out_size
) {
    UART_write(uart, msg, msg_size);
    System_printf("--> %s", msg);
    int read = UART_read(uart, out, out_size);
    System_printf("%s", out); System_flush();
    return read;
}


inline void util_print_bytes (const char* bytes, size_t bytes_len) {
    int i;
    for (i = 0; i < bytes_len; i++) {
        if (i > 0) System_printf(":");
        System_printf("%02X", bytes[i]);
    }
}


/**
 * Send an ack packet
 *
 * @param packet packet sent from the server
 * */
void util_ack  (Packet* packet);


/**
 * Send an nack packet
 *
 * @param packet packet sent from the server
 * */
void util_nack (Packet* packet);


/**
 * Send an error packet
 *
 * @param err error code
 * */
void util_err (ErrorCode err);



/**
 * Convert time to format used to send in packet to server
 *
 * @param t the time to represent
 *
 * */
uint64_t util_mk_time (time_t t);



#endif // __OURICO_WQM_UTIL_H__
