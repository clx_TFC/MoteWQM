#ifndef __OURICO_WQM_PROTOCOL_H__
#define __OURICO_WQM_PROTOCOL_H__


#include <stdlib.h>
#include <string.h>

#include "crypto.h"
#include "config.h"


#define PROTOCOL_MAX_PACKET_SIZE 512 // bytes

#define PROTOCOL_FLAGS__ASYNC_MEASUREMENT 0x01

#define PROTOCOL_INTERFACE_NUM_SIZE 1 // bytes



typedef enum {

    ACK                   = 0,
    NACK                  = 1,
    ERROR                 = 2,

    // packets sent from the mote
    MEASUREMENTS          = 3,
    STATUS_REPORT         = 4,
    ACTION_RESULT         = 5,
    INIT                  = 6,
    CONF_CHANGE           = 7,
    ALERT                 = 8,
    TRIGGER               = 9,

    // packets sent from the server
    CONF_SET              = 10,
    ACTION_INSTR          = 11,
    REQUEST_STATUS_REPORT = 12,
    REQUEST_MEASUREMENTS  = 13,
    ALERT_SET             = 14,
    ALERT_REMOVE          = 15,
    TRIGGER_SET           = 16,
    TRIGGER_REMOVE        = 17,
    SENSOR_ACTIVATE       = 18,
    SENSOR_DEACTIVATE     = 19,
    ACTUATOR_ACTIVATE     = 20,
    ACTUATOR_DEACTIVATE   = 21,
    RESET                 = 22,

} PacketType;


/**
 * Lengths of fields in each packet type
 * */

extern const int HEADER_FIELDS_COUNT;
extern const int HEADER_LENGTH;
extern const int HEADER_FIELDS[];

extern const int ACK_FIELDS_COUNT;
extern const int ACK_FIELDS[];

extern const int NACK_FIELDS_COUNT;
extern const int NACK_FIELDS[];

extern const int ERROR_FIELDS_COUNT;
extern const int ERROR_FIELDS[];

extern const int MEASUREMENTS_FIELDS_COUNT;
extern const int MEASUREMENTS_FIELDS[];

extern const int STATUS_REPORT_FIELDS_COUNT;
extern const int STATUS_REPORT_FIELDS[];

extern const int ACTION_RESULT_FIELDS_COUNT;
extern const int ACTION_RESULT_FIELDS[];

extern const int INIT_FIELDS_COUNT;
extern const int INIT_FIELDS[];

extern const int CONF_CHANGE_FIELDS_COUNT;
extern const int CONF_CHANGE_FIELDS[];

extern const int ALERT_FIELDS_COUNT;
extern const int ALERT_FIELDS[];

extern const int TRIGGER_FIELDS_COUNT;
extern const int TRIGGER_FIELDS[];

extern const int CONF_SET_FIELDS_COUNT;
extern const int CONF_SET_FIELDS[];

extern const int ACTION_INSTR_FIELDS_COUNT;
extern const int ACTION_INSTR_FIELDS[];

extern const int REQUEST_STATUS_REPORT_FIELDS_COUNT;
extern const int REQUEST_STATUS_REPORT_FIELDS[];

extern const int REQUEST_MEASUREMENTS_FIELDS_COUNT;
extern const int REQUEST_MEASUREMENTS_FIELDS[];

extern const int ALERT_SET_FIELDS_COUNT;
extern const int ALERT_SET_FIELDS[];

extern const int ALERT_REMOVE_FIELDS_COUNT;
extern const int ALERT_REMOVE_FIELDS[];

extern const int TRIGGER_SET_FIELDS_COUNT;
extern const int TRIGGER_SET_FIELDS[];

extern const int TRIGGER_REMOVE_FIELDS_COUNT;
extern const int TRIGGER_REMOVE_FIELDS[];

extern const int SENSOR_ACTIVATE_FIELDS_COUNT;
extern const int SENSOR_ACTIVATE_FIELDS[];

extern const int SENSOR_DEACTIVATE_FIELDS_COUNT;
extern const int SENSOR_DEACTIVATE_FIELDS[];

extern const int ACTUATOR_ACTIVATE_FIELDS_COUNT;
extern const int ACTUATOR_ACTIVATE_FIELDS[];

extern const int ACTUATOR_DEACTIVATE_FIELDS_COUNT;
extern const int ACTUATOR_DEACTIVATE_FIELDS[];

extern const int RESET_FIELDS_COUNT;
extern const int RESET_FIELDS[];


typedef uint64_t field_t;

typedef struct {

    field_t value;
    int     byte_len;

} PacketField;


typedef struct {

    int          mote_id;
    PacketType   type;
    size_t       length;
    UInt         number;
    PacketField* fields;
    int          nr_fields;

} Packet;



/**
 * Parse a packet. Decrypts the packet if encryption is
 * enabled in the current configuration
 *
 * @param packet     the raw packet received
 * @param packet_len length of `packet` in bytes
 *
 * @return the packet representation
 *
 * */
Packet* protocol_parse_packet (char* packet, size_t packet_len);



/**
 * Build a packet. The packet is encrypted if encryption is enabled
 * in the current configuration.
 *
 * With encryption the packet format will be
 * +--------+-------------------+-----+-------+
 * | header | encrypted payload | mac | nonce |
 * +--------+-------------------+-----+-------+
 * The header will be 2 byte authenticated sequence (the mote id) in the
 * beggining of the packet. The remaining header fields
 * (packet number, packet type and length) will be included
 * in the encrypted payload
 *
 * Without encryption it will be
 * +------------------+
 * | header | payload |
 * +--------+---------+
 * With the normal 6 byte header (mote id,
 * packet number, packet type, length)
 *
 * @param packet fields for the packet
 * @param output buffer where to store result (will be allocated)
 *
 * @return the size of `output`
 *
 * */
size_t protocol_make_packet (Packet* packet, char** output);



inline Packet* protocol_alocate_packet (
    int        mote_id,
    PacketType type,
    UInt       number,
    int        nr_fields
) {
    Packet* result    = malloc(sizeof(Packet));
    result->mote_id   = mote_id;
    result->type      = (PacketType) type;
    result->number    = number;
    result->nr_fields = nr_fields;
    result->fields    = malloc(nr_fields * sizeof(PacketField));
    return result;
}


inline void protocol_free_packet (Packet* packet) {
    if (packet != NULL) {
        if (packet->fields != NULL)
            free(packet->fields);
        free(packet);
    }
}


#endif // __OURICO_WQM_PROTOCOL_H__
