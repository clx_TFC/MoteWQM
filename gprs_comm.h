#ifndef __OURICO_WQM_GSM_COMM__H__
#define __OURICO_WQM_GSM_COMM__H__

/**
 * GSM/GPRS communcation
 * */



#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>

#include <xdc/std.h>
#include <xdc/runtime/System.h>

#include <ti/drivers/PIN.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/uart/UARTCC26XX.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "Board.h"

#include "util.h"
#include "protocol.h"



/**
 * Type of callback to be called when a new packet is received
 *
 * Receives packet and packet length in bytes
 * */
typedef void (gprs_comm_rx_callback_t) (const char*, size_t);


/**
 * IPv4 representation
 * a.b.c.d
 * */
typedef struct {
    UChar a;
    UChar b;
    UChar c;
    UChar d;
} IP;


/**
 * Some AT Commands
 * */
#define AT_AT                    "AT\r"
#define AT_DISABLE_ECHO          "ATE0\r"
#define AT_ENABLE_VERBOSE_ERRORS "AT+CMEE=2\r"
#define AT_GPRS_ATTACH           "AT+CGATT=1\r"
#define AT_ADD_PDP_CONTEXT       "AT+CGDCONT=1,\"IP\",\"default\"\r"
#define AT_ACTIVATE_PDP_CONTEXT  "AT#SGACT=1,1\r"
#define AT_SOCKET_CONFIG         "AT#SCFG=3,1,300,90,600,50\r"
#define AT_SOCKET_CONFIG_EXT     "AT#SCFGEXT=3,1,0,2\r" // keepalives every 2 minutes
#define AT_GET_ASSIGNED_IP       "AT+CGPADDR=1\r"
#define AT_IS_CONNECTED          "AT#SS=3\r"
#define AT_PAUSE_CONNECTION      "+++"
#define AT_RESUME_CONNECTION     "AT#SO=3\r"
#define AT_READ_DATA             "AT#SRECV=3,512\r" // XXX: hardcoded max size

#define MODULE_LAG 5000000/Clock_tickPeriod
#define MAX_IN_BUFFER_SZ 512


/**
 * Initialize the GSM/GPRS module
 *
 * */
void gprs_comm_init ();



/**
 * Connect to server
 *
 * @param server_addr server IP address
 * @param server_port port where to connect
 *
 * @return 0 if connected, -1 if failed
 * */
bool gprs_comm_connect (const char *server_addr, int server_port);



/**
 * Send a packet
 *
 * @param packet      the packet to send
 * @param packet_size size of `packet` array
 *
 * @return true if successfully set, false otherwise
 * */
bool gprs_comm_send (const char* packet, size_t packet_size);



/**
 * Read a new message in polling mode
 *
 * @param buffer     to store received data
 * @param buffer_len size of `buffer` in bytes
 *
 * @return number of bytes read of -1 if nothing was read
 *
 * */
int gprs_comm_recv (char* buffer, size_t buffer_len);



/**
 * Get the IP address assigned by the ISP
 *
 * @return the ip representation
 *
 * */
IP gprs_comm_get_assigned_ip ();



/**
 * Check if the module is connected to the server
 *
 * @return 1 if yes 0 if no
 * */
bool gprs_comm_is_connected ();



#endif // __OURICO_WQM_GSM_COMM__H__
