
#include <xdc/std.h>
#include <xdc/runtime/System.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>

#include <ti/drivers/PIN.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/uart/UARTCC26XX.h>
#include <ti/drivers/crypto/CryptoCC26XX.h>
#include <driverlib/ioc.h>

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "Board.h"

#include "gprs_comm.h"
#include "config.h"
#include "protocol.h"
#include "util.h"
#include "sensors.h"
#include "stats.h"
#include "handlers.h"


#define TASKSTACKSIZE   768
#define SLEEP_TIME_SECS 30
#define SLEEP_TIME      (SLEEP_TIME_SECS*1000000)/Clock_tickPeriod



Task_Struct task_start_struct;
Task_Struct task_read_struct;
Task_Struct task_write_struct;
Char task0Stack[TASKSTACKSIZE];
Char task1Stack[TASKSTACKSIZE];
Char task2Stack[TASKSTACKSIZE];


static PIN_Handle ledPinHandle;
static PIN_State ledPinState;

PIN_Config ledPinTable[] = {
    Board_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    Board_LED3 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    Board_LED4 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};


static int packet_no    = 0;
static bool initialized = false;
// timers
static int mp_timer = 0; // measure
static int sd_timer = 0; // send data
static int sr_timer = 0; // status report


bool init () {

    IP ip = gprs_comm_get_assigned_ip();

    if (!gprs_comm_connect(CONF_SERVER_ADDRESS, CONF_SERVER_PORT))
        return false;

    Packet packet;
    packet.mote_id   = CONF_THIS_MOTE_ID;
    packet.number    = packet_no++;
    packet.type      = INIT;
    packet.nr_fields = 1;
    packet.fields    = malloc(packet.nr_fields * sizeof(PacketField));
    packet.fields[0].value  = ip.d;
    packet.fields[0].value += ip.c << 8;
    packet.fields[0].value += ip.b << 16;
    packet.fields[0].value += ip.a << 24;
    packet.fields[0].byte_len = 4;
    char* pt;
    size_t plen = protocol_make_packet(&packet, &pt);

    gprs_comm_send(pt, plen);
    free(pt);

    return true;
}


void start () {
    // wait for GSM module to start
    Task_sleep((20000000)/Clock_tickPeriod);

    gprs_comm_init();
    stats_init();
    crypto_init();

    PIN_setOutputValue(ledPinHandle, Board_LED3, 1);
    initialized = init();
    if (!initialized)
        return;
    PIN_setOutputValue(ledPinHandle, Board_LED4, 1);
}


Void write (UArg a, UArg b) {
    while (!initialized) { Task_sleep(1000000); }

    while (true) {

        int measurement_period = conf_get(MEASUREMENT_PERIOD);
        int send_data_period   = conf_get(SEND_DATA_PERIOD);
        int report_period      = conf_get(STATUS_REPORT_PERIOD);

        if (mp_timer >= measurement_period) {
            sensors_do_measure();
            mp_timer = 0;
        }
        if (sd_timer >= send_data_period) {
            //PIN_setOutputValue(ledPinHandle, Board_LED4, 1);
            handle_send_measurements(packet_no++);
            sd_timer = 0;
        }
        if (sr_timer >= report_period) {
            //PIN_setOutputValue(ledPinHandle, Board_LED4, 0);
            handle_send_status_report(packet_no++);
            sr_timer = 0;
        }

        Task_sleep(SLEEP_TIME);
        mp_timer += SLEEP_TIME_SECS;
        sd_timer += SLEEP_TIME_SECS;
        sr_timer += SLEEP_TIME_SECS;
    }
}


Void read (UArg a, UArg b) {
    while (!initialized) { Task_sleep(1000000); }

    char* in = malloc(PROTOCOL_MAX_PACKET_SIZE);
    int in_len;

    while (true) {
        memset(in, 0, PROTOCOL_MAX_PACKET_SIZE);
        if ((in_len = gprs_comm_recv(in, PROTOCOL_MAX_PACKET_SIZE)) != -1) {
            //System_printf("%d, %s", in_len, in); System_flush();
            PIN_setOutputValue(ledPinHandle, Board_LED1, 0);
            Packet* packet = protocol_parse_packet(in, in_len);
            if (packet == NULL) {
                util_err(RECEIVED_MALFORMED_PACKET);
            } else {
                handle_packet(packet, in, in_len);
            }
            protocol_free_packet(packet);
        } else { PIN_setOutputValue(ledPinHandle, Board_LED1, 1); }

        //Task_sleep(SLEEP_TIME);
    }
}



int main (void) {
    Task_Params task_start, task_read, task_write;

    Board_initGeneral();
    Board_initUART();
    CryptoCC26XX_init();

    Task_Params_init(&task_start);
    Task_Params_init(&task_read);
    Task_Params_init(&task_write);
    task_start.stackSize = TASKSTACKSIZE;
    task_read.stackSize = TASKSTACKSIZE;
    task_write.stackSize = TASKSTACKSIZE;
    task_start.stack = &task0Stack;
    task_read.stack = &task1Stack;
    task_write.stack = &task2Stack;

    Task_construct(&task_start_struct, (Task_FuncPtr)start, &task_start, NULL);
    Task_construct(&task_read_struct,  (Task_FuncPtr)read,  &task_read, NULL);
    Task_construct(&task_write_struct, (Task_FuncPtr)write, &task_write, NULL);

    ledPinHandle = PIN_open(&ledPinState, ledPinTable);
    if(!ledPinHandle) {
        System_abort("Error initializing board LED pins\n");
    }

    BIOS_start();

    return 0;
}
